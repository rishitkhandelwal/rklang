use parser::{parse, Machine};

fn main() {
    let code = std::fs::read_to_string("main.rk").unwrap();

    let instructions = parse(code);
    let mut mach = Machine::new(instructions);

    mach.run();
}
