use std::{collections::HashMap, slice::Iter};

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Instruction {
    Byte(i32),
    RegByte1,
    RegByte2,
    UnsetRegByte1,
    UnsetRegByte2,

    Push,
    Pop,
    Copy,

    Print,

    Add,
    Sub,
    Mul,
    Div,
    EqualTo,

    Loop,

    If,
    End,
}

pub fn parse(code: String) -> Vec<Instruction> {
    let mut result = vec![];

    let tokens: Vec<&str> = code.split(" ").collect();
    for tok in tokens {
        let tok = tok.trim();
        handle_token(&mut result, tok.to_string());
    }
    result
}

fn handle_token(result: &mut Vec<Instruction>, token: String) {
    match token.as_str() {
        "push" => result.push(Instruction::Push),
        "pop" => result.push(Instruction::Pop),
        "+" => result.push(Instruction::Add),
        "-" => result.push(Instruction::Sub),
        "*" => result.push(Instruction::Mul),
        "/" => result.push(Instruction::Div),
        "==" => result.push(Instruction::EqualTo),
        "cpy" => result.push(Instruction::Copy),
        "print" => result.push(Instruction::Print),
        "if" => result.push(Instruction::If),
        "loop" => result.push(Instruction::Loop),
        "end" => result.push(Instruction::End),

        "rg1" => result.push(Instruction::RegByte1),
        "rg2" => result.push(Instruction::RegByte2),

        "urg1" => result.push(Instruction::UnsetRegByte1),
        "urg2" => result.push(Instruction::UnsetRegByte2),

        misc => {
            let num = misc.parse::<i32>();

            if let Ok(num) = num {
                result.push(Instruction::Byte(num))
            } else {
                let tryagain = misc.split_whitespace();
                for tok in tryagain {
                    handle_token(result, tok.to_string());
                }
            }
        }
    };
}

pub struct Machine {
    stack: Vec<i32>,
    code: Vec<Instruction>,
    registers: HashMap<String, i32>,
    current_pos: usize,
    expected_end_count: u32,
}

impl Machine {
    pub fn new(code: Vec<Instruction>) -> Self {
        Self {
            stack: vec![],
            code,
            registers: HashMap::new(),
            current_pos: 0,
            expected_end_count: 0,
        }
    }

    pub fn run(&mut self) {
        let code = self.code.clone();
        let mut code_iter = code.iter();

        while let Some(inst) = code_iter.next() {
            self.exec_inst(*inst, &mut code_iter);
            self.current_pos += 1;
        }
    }

    fn exec_inst(&mut self, inst: Instruction, code_iter: &mut Iter<Instruction>) {
        match inst {
            Instruction::RegByte1 => {
                let reg = self.registers.get("rg1");

                if let Some(reg) = reg {
                    self.stack.push(*reg);
                } else {
                    let item = self.stack.pop();
                    if let Some(item) = item {
                        self.registers.insert("rg1".to_string(), item);
                    } else {
                        panic!("Register item not defined");
                    }
                }
            }
            Instruction::RegByte2 => {
                let reg = self.registers.get("rg2");

                if let Some(reg) = reg {
                    self.stack.push(*reg);
                } else {
                    let item = self.stack.pop();
                    if let Some(item) = item {
                        self.registers.insert("rg2".to_string(), item);
                    } else {
                        panic!("Register item not defined");
                    }
                }
            }

            Instruction::UnsetRegByte1 => {
                eprintln!("unsed reg 1");
                self.registers.remove("rg1");
            }
            Instruction::UnsetRegByte2 => {
                eprintln!("unsed reg 2");
                self.registers.remove("rg2");
            }

            Instruction::Push => {
                let item = code_iter.next();
                if item.is_none() {
                    panic!("Push item not defined {:?} {}", code_iter, self.current_pos);
                }
                if let Instruction::Byte(item) = item.unwrap() {
                    self.stack.push(*item);
                } else {
                    println!("Not a byte {:?}", item.unwrap());
                }
            }

            Instruction::Print => {
                let nbytes = self.stack.pop().expect("Missing printlen");
                let mut bytes = vec![];
                for _ in 0..nbytes {
                    let byte = self.stack.pop().expect("Not enough bytes on stack");
                    bytes.push(byte as u8);
                }
                bytes.reverse();
                let output: String = bytes.iter().map(|v| *v as char).collect();
                print!("{}", output);
            }

            Instruction::Add => {
                let a = self.stack.pop().expect("operand missing");
                let b = self.stack.pop().expect("operand missing");
                self.stack.push(a + b);
            }
            Instruction::Sub => {
                let a = self.stack.pop().expect("operand missing");
                let b = self.stack.pop().expect("operand missing");
                self.stack.push(b - a);
            }
            Instruction::Mul => {
                let a = self.stack.pop().expect("operand missing");
                let b = self.stack.pop().expect("operand missing");
                self.stack.push(a * b);
            }
            Instruction::Div => {
                let a = self.stack.pop().expect("operand missing");
                let b = self.stack.pop().expect("operand missing");
                self.stack.push(b / a);
            }

            Instruction::EqualTo => {
                let a = self.stack.pop().expect("operand missing");
                let b = self.stack.pop().expect("operand missing");
                self.stack.push(if a == b { 1 } else { 0 });
            }

            Instruction::Copy => {
                let a = self.stack.pop().expect("operand missing");
                self.stack.push(a);
                self.stack.push(a);
            }

            Instruction::If => {
                self.expected_end_count += 1;
                let condition = self.stack.pop().expect("Condition does not exist");
                if condition == 0 {
                    for beta in code_iter.next() {
                        if *beta == Instruction::End {
                            self.expected_end_count -= 1;
                        }
                        if self.expected_end_count == 0 {
                            break;
                        }
                    }
                }
            }

            Instruction::Loop => {
                self.expected_end_count += 1;
                let mut condition = self.stack.pop().expect("Condition does not exist");
                if condition == 0 {
                    while let Some(beta) = code_iter.next() {
                        if *beta == Instruction::End {
                            self.expected_end_count -= 1;
                        }
                        if self.expected_end_count == 0 {
                            break;
                        }
                    }
                } else {
                    let mut loopers = vec![];

                    while let Some(beta) = code_iter.next() {
                        if *beta == Instruction::End {
                            self.expected_end_count -= 1;
                        }
                        if self.expected_end_count == 0 {
                            break;
                        }
                        loopers.push(*beta);
                    }

                    eprintln!("{:?}", loopers);

                    while condition != 0 {
                        loopers.iter().for_each(|inst| {
                            self.exec_inst(*inst, code_iter);
                        });
                        condition -= 1;
                    }
                }
            }

            Instruction::Byte(byte) => {
                self.stack.push(byte);
            }

            _ => {
                // panic!("Not implemented")
            }
        }
    }
}
